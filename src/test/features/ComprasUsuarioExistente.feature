# language: pt
@ComprasUsuarioExistente
Funcionalidade: Como um cliente cadastrado no site automationexercise.com
  Eu quero fazer a compra de ao menos três produtos
  Para que eu possa estar bem vestida

  Cenário: Comprar produtos com um usuário existente
	Dado que eu acesse o ecommerce
	E realize o login com um usuario cadastrado
	Quando eu adicionar itens ao carrinho
	E confirmo a compra do carrinho
	E preencho os dados de pagamento
	Entao devo visualizar a mensagem de sucesso