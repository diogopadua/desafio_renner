package br.com.renner.support;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.cucumber.java.Before;
import io.cucumber.java.After;

import java.util.Arrays;

public class DriverFactory {

    private static WebDriver driver;
    private static ChromeOptions options;

    public DriverFactory () {}
    @Before
    public static WebDriver getDriver() {
        if (driver == null) {
            driver = new ChromeDriver();
            options = new ChromeOptions();
            driver.manage().window().maximize();
            options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
            options.setExperimentalOption("excludeSwitches", Arrays.asList("disable-popup-blocking"));
        }
        return driver;
    }
    @After
    public static void killDriver(){
        if(driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
