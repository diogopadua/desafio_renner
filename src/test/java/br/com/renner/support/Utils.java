package br.com.renner.support;

import org.openqa.selenium.JavascriptExecutor;
import static br.com.renner.support.DriverFactory.getDriver;

public class Utils {
    public void scrollDown(){

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        // Scroll Down de 1000 pixels vertical
        js.executeScript("window.scrollBy(0,1000)");
    }
}