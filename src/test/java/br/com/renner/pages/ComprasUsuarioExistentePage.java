package br.com.renner.pages;

import br.com.renner.support.Utils;
import org.openqa.selenium.By;
import java.util.concurrent.TimeUnit;
import static br.com.renner.support.DriverFactory.getDriver;
import static br.com.renner.support.DriverFactory.killDriver;

public class ComprasUsuarioExistentePage {

    Utils utils = new Utils();
    public void acessarPaginaEcommerce() {
        getDriver().get("https://automationexercise.com/");
        getDriver().getTitle().equals("Automation Exercise");
    }

    public void loginUsuarioExistente(){
        getDriver().findElement(By.cssSelector("#header > div > div > div > div.col-sm-8 > div > ul > li:nth-child(4) > a > i")).click();
        getDriver().findElement(By.xpath("//input[@name='email']")).sendKeys("diogopadua@outlook.com");
        getDriver().findElement(By.xpath("//input[@name='password']")).sendKeys("A1234!@#$");
        getDriver().findElement(By.xpath("//button[@data-qa='login-button']")).click();
        getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        getDriver().findElement(By.xpath("//b[text()='DIOGO DE PADUA BEZERRA DA SILVA']")).getText().equals("DIOGO DE PADUA BEZERRA DA SILVA");
    }
    public void adicionarItensCarrinho(){

        //Adicionando Stylish Dress
        getDriver().findElement(By.xpath("(//ul[@class='nav navbar-nav']//a)[2]")).click();
        getDriver().findElement(By.id("search_product")).sendKeys("Stylish Dress");
        getDriver().findElement(By.xpath("//i[@class='fa fa-search']")).click();
        utils.scrollDown();
        getDriver().findElement(By.xpath("//a[@href='/product_details/4']")).click();
        getDriver().findElement(By.id("quantity")).clear();
        getDriver().findElement(By.id("quantity")).sendKeys("3");
        getDriver().findElement(By.xpath("//button[@type='button']")).click();
        getDriver().findElement(By.xpath("//button[contains(@class,'btn btn-success')]")).click();

        //Adicionando Blue Cotton Linen Saree
        getDriver().findElement(By.xpath("(//ul[@class='nav navbar-nav']//a)[2]")).click();
        getDriver().manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        getDriver().findElement(By.id("search_product")).clear();
        getDriver().findElement(By.id("search_product")).sendKeys("Beautiful Peacock Blue Cotton Linen Saree");
        getDriver().findElement(By.xpath("//i[@class='fa fa-search']")).click();
        utils.scrollDown();
        getDriver().findElement(By.xpath("//a[@href='/product_details/41']")).click();
        getDriver().findElement(By.id("quantity")).clear();
        getDriver().findElement(By.id("quantity")).sendKeys("2");
        getDriver().findElement(By.xpath("//button[@type='button']")).click();
        getDriver().findElement(By.xpath("//button[contains(@class,'btn btn-success')]")).click();

        //Adicionando Men Tshirt
        getDriver().findElement(By.xpath("(//ul[@class='nav navbar-nav']//a)[2]")).click();
        getDriver().manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        getDriver().findElement(By.id("search_product")).clear();
        getDriver().findElement(By.id("search_product")).sendKeys("Men Tshirt");
        getDriver().findElement(By.xpath("//i[@class='fa fa-search']")).click();
        utils.scrollDown();
        getDriver().findElement(By.xpath("//a[@href='/product_details/2']")).click();
        getDriver().findElement(By.id("quantity")).clear();
        getDriver().findElement(By.id("quantity")).sendKeys("1");
        getDriver().findElement(By.xpath("//button[@type='button']")).click();
        getDriver().findElement(By.xpath("//button[contains(@class,'btn btn-success')]")).click();
    }
    public void realizaCompraCarrinho() {
        getDriver().findElement(By.xpath("//a[@href='/view_cart']")).click();
        getDriver().findElement(By.linkText("Proceed To Checkout")).click();
        utils.scrollDown();
        getDriver().findElement(By.xpath("//a[contains(@class,'btn btn-default')]")).click();
    }
    public void preencheDadosCartao(){
        getDriver().findElement(By.xpath("//input[@data-qa='name-on-card']")).sendKeys("USER TEST RENNER");
        getDriver().findElement(By.xpath("//input[@class='form-control card-number']")).sendKeys("5559 0054 3559 2103");
        getDriver().findElement(By.xpath("//input[@class='form-control card-cvc']")).sendKeys("153");
        getDriver().findElement(By.xpath("//input[@class='form-control card-expiry-month']")).sendKeys("12");
        getDriver().findElement(By.xpath("//input[@class='form-control card-expiry-year']")).sendKeys("2030");
        getDriver().findElement(By.id("submit")).click();
    }
    public void checarMensagem(){
        getDriver().findElement(By.xpath("//p[text()='Congratulations! Your order has been confirmed!']")).getText().equals("Congratulations! Your order has been confirmed!");
        getDriver().findElement(By.xpath("//b[text()='Order Placed!']")).getText().equals("Order Placed!");
        killDriver();
    }
}
