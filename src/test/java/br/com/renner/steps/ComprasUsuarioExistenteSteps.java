package br.com.renner.steps;

import br.com.renner.pages.ComprasUsuarioExistentePage;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class ComprasUsuarioExistenteSteps {

    ComprasUsuarioExistentePage comprasPage = new ComprasUsuarioExistentePage();

    @Dado("que eu acesse o ecommerce")
    public void AccessPageEcommerce() {
        comprasPage.acessarPaginaEcommerce();
    }

    @E("realize o login com um usuario cadastrado") public void realizarLogin() {
        comprasPage.loginUsuarioExistente();
    }
    @Quando("eu adicionar itens ao carrinho") public void adicionarItensCarrinho(){
        comprasPage.adicionarItensCarrinho();
    }
    @E("confirmo a compra do carrinho") public void confirmaCompraItensCarrinho() {
        comprasPage.realizaCompraCarrinho();
    }
    @E("preencho os dados de pagamento") public void preencherDadosCartao(){
        comprasPage.preencheDadosCartao();
    }
    @Entao("devo visualizar a mensagem de sucesso") public void checkMensagem(){
        comprasPage.checarMensagem();
    }
}

